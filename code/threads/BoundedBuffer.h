#ifndef BOUNDEDBUFFER_H
#define BOUNDEDBUFFER_H
#include "synch.h"
#include "list.h"
#include "main.h"
#include "kernel.h"
class BoundedBuffer {
public:
  // Create a bounded buffer to hold at most maxsize bytes
  BoundedBuffer(int maxsize);

  ~BoundedBuffer();

  // Read size bytes from the buffer, blocking as necessary until
  // enough bytes are available to completely satisfy the request.
  // Copy the bytes into memory
  // starting at address data
  // Return the number of bytes successfully read
  int Read(void *data, int size);

  // Write size bytes into the buffer, blocking as necessary until
  // enough space is available to completely satisfy the request
  // Copy the bytes from memory
  // starting at address data
  // Return the number of bytes successfully written
  int Write(void *data, int size);

  // Permanently close the BoundedBuffer object
  // This releases any blocked readers or writers
  // any subsequent attempts to Read or Write the buffer fail and return 0.
  void Close();

  void printbuffer();

  void SelfTest(int istest);
  int MaxSize; // bounded buffer's max size
private:
  int cn; // # of datas in bounded buffer
  int in; // position to put data. if data puts, increase
  // if in==MaxSize go back to 0 and put data again
  // if bounded buffer is full, in infinite loop wait and can't put data until out execute
  int out; // position to read data. if data reads, decrease
  // if bounded buffer is empty, out can't execute and execute infinite loop until in put data

  char *buffer;
  Lock *lock;
  Condition *notEmpty;
  Condition *notFull;
  bool isClose; // for Close func

};

#endif
