#include "BoundedBuffer.h"
#include "synch.h"
#include "thread.h"
#include <stdio.h>

// Create a bounded buffer to hold at most maxsize bytes
BoundedBuffer::BoundedBuffer(int maxsize)
{
	MaxSize = maxsize;
	buffer = new char[MaxSize];
	lock = new Lock("list lock");
	notEmpty = new Condition("list empty cond");
	notFull = new Condition("list full cond");
	cn = in = out = 0;
	isClose = true;
}

BoundedBuffer::~BoundedBuffer()
{
	delete buffer;
	delete lock;
	delete notEmpty;
	delete notFull;
}

// Read size bytes from the buffer, blocking as necessary until
// enough bytes are available to completely satisfy the request.
// Copy the bytes into memory
// starting at address data
// Return the number of bytes successfully read

int BoundedBuffer::Read(void *data,int size)
{
	int i;
	lock->Acquire();
	for(i = 0; i < size; i++){
		while(cn == 0 || !isClose)
			notFull->Wait(lock);
		cout <<"Read " << size << " i: " << i << ", out: " << out;
		cout << " " << *((char *)buffer + out) << endl;
		*((char *)data + i) = *((char *)buffer + out);
		if(i == size - 1)
			printf("Read size: %d\n", size);
		out = (out + 1) % MaxSize; // because it is circular buffer
		cn--;
		if(cn == size - i - 1)
			notEmpty->Signal(lock); // increase more data
	}
	lock->Release();
	return size;
}

// Write size bytes into the buffer, blocking as necessary until
// enough space is available to completely satisfy the request
// Copy the bytes from memory starting at address data
// Return the number of bytes successfully written

int BoundedBuffer::Write(void *data,int size)
{
	int j;
	lock->Acquire();
	for(j = 0; j < size; j++){
		while(cn == MaxSize || !isClose) {
			notEmpty->Wait(lock);
			j = 0;
		}
		*((char *)buffer + in) = *((char *)data + j);
		printf("Wirte: %c\n",*((char *)data +j));
		if(j == size-1)
			printf("Write size: %d\n", size);
		in = (in+1) % MaxSize;
		cn++;
		notFull->Signal(lock);
	}
	lock->Release();
	return j;
}

// Permanently close the BoundedBuffer object
// This releases any blocked readers or writers
// any subsequent attempts to Read or Write the buffer fail and return 0.
void BoundedBuffer::Close()
{
	lock->Acquire();
	isClose = false;
	lock->Release();
	cout << "Close execute" << endl;
}

void BoundedBuffer::printbuffer()
{
	printf("buffer = %s\n",buffer);
}

void ConsumerWorkL(BoundedBuffer* testBuffer)
{
	cout << "ConsumerWorkL" << endl;
	char tmp[6];
	testBuffer->Read((void*)tmp, 6);
	cout << "tmp size: " << sizeof(tmp) << endl;
	/*for(int i = 0; i < sizeof(tmp); i++)
		cout << tmp[i] << "---------" << endl;*/
}

void ConsumerWorkR(BoundedBuffer* testBuffer)
{
	cout << "ConsumerWorkR" << endl;
	char tmp[4];
	testBuffer->Read((void*)tmp, 4);
	/*for(int i = 0; i < sizeof(tmp); i++)
		cout << "---------" << tmp[i] << endl;*/
}

void ProducerWorkChar(BoundedBuffer* testBuffer)
{
	cout << "ProducerWorkChar" << endl;
	char tmp[10];
	for(int i = 0; i < 10; i++) {
		tmp[i] = 'a' + i;
	}
	testBuffer->Write((void*)tmp, 5);
}

void ProducerWorkNum(BoundedBuffer* testBuffer)
{
	char tmp[10];
	for(int i = 0; i < 10; i++) {
		tmp[i] = '0' + i;
	}
	testBuffer->Write((void*)tmp, 3);
}

void CloseTest(BoundedBuffer* testBuffer)
{
	testBuffer->Close();
}

void BoundedBuffer::SelfTest(int istest)
{
	Thread *c1 = new Thread("consumer1");
	Thread *c2 = new Thread("consumer2");
	Thread *p1 = new Thread("producer1");
	Thread *p2 = new Thread("producer2");
	Thread *t_c = new Thread("closeTest");

	c1->Fork((VoidFunctionPtr)ConsumerWorkL, (void*)this);
	p1->Fork((VoidFunctionPtr)ProducerWorkChar, (void*)this);
	c2->Fork((VoidFunctionPtr)ConsumerWorkR, (void*)this);
	p2->Fork((VoidFunctionPtr)ProducerWorkNum, (void*)this);
	if(istest != 0) {
		t_c->Fork((VoidFunctionPtr)CloseTest, (void*)this);
	}
}
